#!/bin/sh

# first, restart (if needed) pcscd
/usr/sbin/pcscd -d
sleep 1
vpcd_ports_list=`/usr/bin/vpcd-config|grep port| awk '{ print $3 }'`
found=0;
notfound=0;

for port in $vpcd_ports_list; do
  /usr/bin/netstat -laputen|grep $port
  if [ $? -eq 0 ]; then
    echo "[+] listening port $port was found"
    found=$(($found + 1));
  else
    echo "[-] listening port $port was not found"
    notfound=$(($notfound + 1));
  fi
done

if [ $notfound -ne 0 ]; then
  echo "[-] at least one listening socket was not found!"
  exit 1;
fi

exit 0;
