#!/bin/sh -e

set -x
# called by uscan with '--upstream-version' <version> <file>
PKG=vsmartcard
DIR=$PKG-$2
TAR=../${PKG}_$2+dfsg.orig.tar.gz

mkdir $DIR
tar --strip-components=1 -C $DIR -xJf ../${PKG}_$2.orig.tar.xz

tar -c -z -f $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR

exit 0
