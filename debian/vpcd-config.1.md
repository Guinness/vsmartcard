% VPCD-CONFIG(1) VPCD-CONFIG Man Page
% Philippe Thierry
% June 2020
# NAME
vpcd-config - dump local VPCD config

# SYNPSIS

**vpcd-config**

# DESCRIPTION

vpcd-config tries to guess the local IP address and outputs vpcd‘s configuration. vpicc‘s options should be chosen accordingly (--hostname and --port).
vpcd-config also prints a QR code for configuration of the Remote Smart Card Reader.

# OPTIONS

none.

# HISTORY

June 2020, Man page originally compiled by Philippe Thierry (philou at debian dot org)
